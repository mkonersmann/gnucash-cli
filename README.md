GnuCash CLI
===========

This is a CLI to GnuCash.

## Setup
To setup the file to handle, use the following command:

```
export GNUCASH_CLI_FILE=/path/to/file
```

## Use

The cli currently supports the following commands:

* ```showall``` - Show the balances of all accounts.
* ```show $account-name``` - Show the balance of a specific account.
* ```extend $from-account-name $to-account-name $amount $comment $date``` - Create a transaction that adds the given amount to both accounts.
* ```exchange $from-account-name $to-account-name $amount $comment $date``` - Create a transaction that moves the given amount from the first to the second account.

Account names are separated by a ```/```. E.g.: ```Assets/Bank```.
Dates must be in the format ```YYYY-MM-DD```.

## Autocompletion

Autocompletion helps with commands and account names.
To use autocomplete in bash do ```source gnucash-cli-complete.bash```.
If you want this to survive a restart, add that to your ```~/.bashrc``` or similar.
