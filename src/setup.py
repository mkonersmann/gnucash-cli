from setuptools import setup, find_packages

setup(
    name='gnucash-cli',
    version='1.0.1',
    url='https://gitlab.com/mkonersmann/gnucash-cli',
    license='EPL-2.0',
    author='Marco Konersmann',
    author_email='mkonersmann@mkonersmann.de',
    description='A command line interface for GnuCash',
    packages=find_packages(include=['mko.gnucash_cli']),
    include_package_data=True,
    install_requires=[
        'Click==8.0.3',
        'pytz==2021.3',
        'gnewcash==1.1.1'
    ],
    entry_points={
        'console_scripts': [
            'gnucash-cli = mko.gnucash_cli.GnuCashCLI:cli'
        ]
    }
)
