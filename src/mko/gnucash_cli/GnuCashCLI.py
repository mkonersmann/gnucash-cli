# coding: utf-8
import os
from datetime import date
from decimal import Decimal

import click
import pytz
from gnewcash import Account, AccountType, GZipXMLFileFormat

from mko.gnucash_cli import Commands
from mko.gnucash_cli.Commands import GnuCashConnection

ACCOUNT_SEPARATOR = "/"
DATE_FORMAT = "%Y-%m-%d"
CLI_FILE_ENV_KEY = "GNUCASH_CLI_FILE"
CLI_FILE_FORMAT = GZipXMLFileFormat


@click.group(invoke_without_command=True)
@click.pass_context
def cli(ctx):
    if ctx.invoked_subcommand is None:
        showAll()


def complete_account(ctx, param, incomplete):
    """
    Autocompletion for account names
    :param ctx: The context. Provided by click.
    :param param: The parameters. Provided by click.
    :param incomplete: The incomplete string to autocomplete. Provided by click.
    :return: A list of candidates for autocompletion.
    """
    if os.environ.get(CLI_FILE_ENV_KEY) is None:
        # Autocomplete only works with the environment variable.
        return []
    conn = connect()
    level = incomplete.count(ACCOUNT_SEPARATOR) + 1

    result = []
    for a in conn.getAccounts():
        fqn = getFQN(a)
        if fqn.count(ACCOUNT_SEPARATOR) >= level:
            continue
        if fqn.startswith(incomplete):
            index = nthIndexOf(fqn, ACCOUNT_SEPARATOR, level)
            if index == -1:
                result.append(fqn)
            else:
                result.append(fqn[0:index])
    return result


def nthIndexOf(text, pattern, n):
    """
    Returns the nth index of pattern in text. Internal operation for complete_account.
    :param text: The text to search in.
    :param pattern: The pattern to search for.
    :param n: The number of instances to search for.
    :return: The character count of the nth instance of pattern in text.
    """
    if n == 1:
        return text.find(pattern)
    else:
        return text.find(pattern, nthIndexOf(text, pattern, n - 1) + 1)


@cli.command()
@click.argument('account', shell_complete=complete_account)
@click.argument('file', envvar=CLI_FILE_ENV_KEY, type=click.File('r'))
def show(account: str, file):
    """
    Show the balance of an account including subaccounts.

    :param account: The account as fully qualified name, separated by '/'. E.g., Aktiva/Bank
    :param file: The file name pointing to the gnucash file. Can be replaced by the environment variable CLI_FILE_ENV_KEY
    """
    conn = connect(file.name)
    balance = conn.showBalance(getAccount(account, conn))
    click.echo(str(balance))


@cli.command()
@click.argument('file', envvar=CLI_FILE_ENV_KEY, type=click.File('r'))
def showAll(file):
    """
    Show the balance of all accounts.

    :param file: The file name pointing to the gnucash file. Can be replaced by the environment variable CLI_FILE_ENV_KEY
    """
    conn = connect(file.name)
    balance = conn.showBalanceAllAccounts()

    # Find the largest absolute number for layouting reasons.
    largest = 0
    for a in balance.keys():
        if largest <= abs(balance[a]):
            largest = abs(balance[a])

    output = ""
    for a in balance.keys():
        if len(a.children) > 0:
            # Accounts with subaccounts should be printed boldly.
            output = output + "\033[7m"

        numberOfSpaces = 1 + len(str(largest))
        amount = getMultiplied(balance[a], a.type)

        amount_str = f'{amount:.2f}'
        for i in range(numberOfSpaces - len(amount_str)):
            output = output + " "

        output = output + amount_str + " €\t\t"

        # Prefix with tabs for each level of subaccount hierarchy.
        for i in range(getFQN(a).count(ACCOUNT_SEPARATOR)):
            output = output + "\t"
        output = output + a.name + "\n"

        if len(a.children) > 0:
            # End boldness of accounts with subaccounts.
            output = output + "\033[0m"
    click.echo(output)


@cli.command()
@click.argument('acc_from', type=click.STRING, shell_complete=complete_account)
@click.argument('acc_to', type=click.STRING, shell_complete=complete_account)
@click.argument('amount', type=click.FLOAT)
@click.argument('comment', type=click.STRING)
@click.argument('tdate', type=click.DateTime(formats=[DATE_FORMAT]), default=str(date.today()))
@click.argument('file', envvar=CLI_FILE_ENV_KEY, type=click.File('r'))
@click.option('--dry-run', '-n', is_flag=True)
def exchange(acc_from: str, acc_to: str, amount: Decimal, comment: str, tdate: str, file, dry_run):
    """
    Executes an exchange transaction.
    The *from* account will get the value subtracted. The *to* account will get the value added.

    :param dry_run: Do not store the changes to the file.
    :param tdate: the date of the transactions. Format: YYYY-MM-DD
    :param comment: A comment to the transaction
    :param amount: The amount of money to move.
    :param acc_to: The account to move money to as FQN, e.g., Assets/Bank/Card
    :param acc_from:The account to move money from as FQN, e.g., Assets/Bank/Card
    :param file: The file name pointing to the gnucash file. Can be replaced by the environment variable CLI_FILE_ENV_KEY.
    """
    conn = connect(file.name)
    tdate = pytz.utc.localize(tdate)  # we need to make the date timezone-aware. Give it UTC
    tid = conn.transactionExchange(tdate=tdate,
                                   amount=amount,
                                   comment=comment,
                                   a_from=getAccount(acc_from, conn),
                                   a_to=getAccount(acc_to, conn))
    if not dry_run:
        conn.store()
    click.echo(tid)


@cli.command()
@click.argument('acc_from', type=click.STRING, shell_complete=complete_account)
@click.argument('acc_to', type=click.STRING, shell_complete=complete_account)
@click.argument('amount', type=click.FLOAT)
@click.argument('comment', type=click.STRING)
@click.argument('tdate', type=click.DateTime(formats=[DATE_FORMAT]), default=str(date.today()))
@click.argument('file', envvar=CLI_FILE_ENV_KEY, type=click.File('r'))
@click.option('--dry-run', '-n', is_flag=True)
def extension(acc_from: str, acc_to: str, amount: Decimal, comment: str, tdate: str, file, dry_run):
    """
    Executes an extension transaction.
    The *from* and *to* account will get the value added.

    :param dry_run: Do not store the changes to the file.
    :param tdate: the date of the transactions. Format: YYYY-MM-DD
    :param comment: A comment to the transaction
    :param amount: The amount of money to move.
    :param acc_to: The account to move money to as FQN, e.g., Assets/Bank/Card
    :param acc_from:The account to move money from as FQN, e.g., Assets/Bank/Card
    :param file: The file name pointing to the gnucash file. Can be replaced by the environment variable CLI_FILE_ENV_KEY.
    """
    conn = connect(file.name)
    tdate = pytz.utc.localize(tdate)  # we need to make the date timezone-aware. Give it UTC
    tid = conn.transactionExtension(tdate=tdate,
                                    amount=amount,
                                    comment=comment,
                                    a_from=getAccount(acc_from, conn),
                                    a_to=getAccount(acc_to, conn))
    if not dry_run:
        conn.store()
    click.echo(tid)


# Utility functions
if __name__ == '__main__':
    # cli is the default group of commands
    cli()


def getFQN(acc: Account):
    """
    Returns the fully qualified name of the given accounts.
    """
    accounts = Commands.getHierarchy(acc)
    name = ACCOUNT_SEPARATOR.join(map(getName, accounts))
    return name


def getName(acc: Account):
    """
    Returns the name of the given account. For the fully qualified name use getFQN(acc).
    """
    return acc.name


def getAccount(fqn: str, conn: GnuCashConnection):
    """
    Returns the account object as defined by the given fully qualified name.
    """
    accs = fqn.split(ACCOUNT_SEPARATOR)
    return conn.getAccount(accs)


def getMultiplied(amount: float, atype: AccountType):
    """
    Liability, credit, and income type accounts are represented in negative numbers.
    E.g., increasing the income means to lower its value.
    This operation returns the respective multiplier -1 for these account types and 1 for all others.
    """
    # Never return -0
    if amount == 0:
        return 0

    mult: int = 1
    if atype == AccountType.LIABILITY or \
            atype == AccountType.CREDIT or \
            atype == AccountType.INCOME or \
            atype == AccountType.EQUITY or \
            atype.__str__() == "PAYABLE":
        # There is no AccountType.PAYABLE
        mult = -1
    return amount * mult


def connect(file: str = None):
    """
    Returns a connection from the CLI to the commands.

    :param file: (Optional) path to the gnucash file. If non given, the environment variable given by
    CLI_FILE_ENV_KEY is used.
    """
    if file is None:
        file = os.getenv(CLI_FILE_ENV_KEY)
    return GnuCashConnection(file, CLI_FILE_FORMAT)
