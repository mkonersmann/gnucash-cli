# coding: utf-8
from datetime import *
from decimal import Decimal

from gnewcash import GnuCashFile, Account, Transaction, Split, GuidObject


def getHierarchy(acc: Account):
    """
    Returns the account hierarchy for the given account as list.
    E.g., in a hierarchy A/B/C, when giving B, this returns A/B.
    """
    hierarchy = [acc]
    if acc.parent is None:
        return hierarchy

    while acc.parent is not None and acc.parent.type != "ROOT":
        acc = acc.parent
        hierarchy.append(acc)
    hierarchy.reverse()
    return hierarchy


class GnuCashConnection:
    """
    Execute commands upon a gnucash file.
    """

    def __init__(self, file_name: str, file_format):
        """
        :param file_name: The path to the gnucash file.
        :param file_format: Either gnewcash.GZipXMLFileFormat or gnewcash.XMLFileFormat
        """
        self.filename = file_name
        self.gc_file = GnuCashFile.read_file(file_name, file_format)
        self.book = self.gc_file.books[0];
        self.tz = timezone.utc

    def store(self):
        """
        Store the changes to the gnucash file.
        """
        self.gc_file.build_file(self.filename, self.file_format)

    def showBalanceAllAccounts(self, acc: Account = None, balances=None):
        """
        Returns the balance of all accounts in the file as a dictionary.
        :param acc: The current account in the hierarchy. Only used internally for recursion.
        :param balances: The current dictionary. Only used internally for recursion.
        """
        if acc is None:
            acc = self.book.root_account

        if acc is self.book.root_account:
            balances = {}
        else:
            balances[acc] = self.showBalance(acc)

        for a in acc.children:
            self.showBalanceAllAccounts(a, balances)

        return balances

    def showBalance(self, acc: Account, recursively: bool = True):
        """
        Returns the balance for the given account.
        :param acc: The account.
        :param recursively: Whether to add up all childrens' balances. Defaults to True.
        :return:
        """
        if recursively:
            balance = self.getBalanceRecursively(acc)
        else:
            balance = self.book.get_account_balance(acc)
        return balance

    def getBalanceRecursively(self, acc: Account):
        """
        Internal method for showBalance.
        """
        balance = self.book.get_account_balance(acc)
        for a in acc.children:
            balance += self.getBalanceRecursively(a)
        return balance

    def transactionExchange(self, a_from: Account, a_to: Account, amount: Decimal, tdate: date, comment: str,
                            tid: str = None):
        """
        Executes an exchange transaction.
        The *from* account will get the value subtracted. The *to* account will get the value added.

        :param tid: a transaction id. If none given, creates a new one.
        :param tdate: the date of the transactions. Format: YYYY-MM-DD
        :param comment: A comment to the transaction
        :param amount: The amount of money to move.
        :param a_to: The account to move money to as FQN, e.g., Assets/Bank/Card
        :param a_from:The account to move money from as FQN, e.g., Assets/Bank/Card
        :param file: The file name pointing to the gnucash file. Can be replaced by the environment variable CLI_FILE_ENV_KEY.
        :return: The transaction id
        """
        transaction = self.prepareTransaction(comment=comment, tdate=tdate, transaction_id=tid)

        transaction.splits = [
            Split(a_from, -1 * amount),
            Split(a_to, amount)
        ]
        self.book.transactions.add(transaction)
        return transaction.memo  # The transaction id

    def transactionExtension(self, a_from: Account, a_to: Account, amount: Decimal, tdate: date, comment: str,
                             tid: str = None):
        """
        Executes an extension transaction.
        The *from* and *to* account will get the value added.

        :param tid: a transaction id. If none given, creates a new one.
        :param tdate: the date of the transactions. Format: YYYY-MM-DD
        :param comment: A comment to the transaction
        :param amount: The amount of money to move.
        :param a_to: The account to move money to as FQN, e.g., Assets/Bank/Card
        :param a_from:The account to move money from as FQN, e.g., Assets/Bank/Card
        :param file: The file name pointing to the gnucash file. Can be replaced by the environment variable CLI_FILE_ENV_KEY.
        :return: The transaction id
        """
        transaction = self.prepareTransaction(comment=comment, tdate=tdate, transaction_id=tid)

        transaction.splits = [
            Split(a_from, amount),
            Split(a_to, amount)
        ]
        self.book.transactions.add(transaction)
        return transaction.memo  # The transaction id

    # Utility Operations
    def prepareTransaction(self, comment: str, tdate: date, transaction_id: str = None):
        """
        Prepares a transaction to be filled.

        :param comment: A comment to the transaction
        :param tdate: the date of the transactions. Format: YYYY-MM-DD
        :param transaction_id: a transaction id. If none given, creates a new one.
        :return: A prepared transaction
        """
        gnucash_transaction = Transaction()
        gnucash_transaction.date_posted = tdate
        gnucash_transaction.date_entered = datetime.now(self.tz)
        gnucash_transaction.description = comment
        if transaction_id is None:
            transaction_id = GuidObject.get_guid()
        gnucash_transaction.memo = transaction_id
        return gnucash_transaction

    def getAccount(self, path_to_account: list):
        """
        Returns the account given by the path.
        :param path_to_account: Path to the account. E.g.: ["Assets", "Bank"]
        """
        return self.book.get_account(*path_to_account)

    def getAccounts(self, acc: Account = None, accounts=None):
        """
        Returns a list of all accounts.
        :param acc: The current account. Used internally for recursion.
        :param accounts: The current list of accounts. Used internally for recursion.
        """
        if acc is None:
            acc = self.book.root_account

        if acc is self.book.root_account:
            accounts = []
        else:
            accounts.append(acc)

        for a in acc.children:
            self.getAccounts(a, accounts)

        return accounts
