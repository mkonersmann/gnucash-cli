# coding: utf-8
import datetime
import os

from mko.gnucash_cli.Commands import *
from gnewcash import GZipXMLFileFormat

path = os.path.abspath(__file__)[0:-1 * len("test_Commands.py")]
TEST_FILE = path + 'gnucash_cli-test-file.gnucash'
CLI_FILE_FORMAT = GZipXMLFileFormat


def test_show_balance():
    conn = GnuCashConnection(TEST_FILE, CLI_FILE_FORMAT)
    assert conn.showBalance(conn.getAccount(["Aufwendungen", "Haus", "Bau"])) == Decimal('150000'), "Balance not ok"
    assert conn.showBalance(conn.getAccount(["Aufwendungen", "Haus", "Wartung"])) == Decimal('0'), "Balance not ok"
    assert conn.showBalance(conn.getAccount(["Aufwendungen", "Haus"])) == Decimal('150000'), "Balance not ok"
    assert conn.showBalance(conn.getAccount(["Aufwendungen"])) == Decimal('155000'), "Balance not ok"


def test_show_all_balances():
    conn = GnuCashConnection(TEST_FILE, CLI_FILE_FORMAT)
    result = conn.showBalanceAllAccounts()
    assert len(result) == 19, "Number of accounts is wrong"
    assert result[conn.getAccount(["Aktiva"])] == 10000
    assert result[conn.getAccount(["Aufwendungen"])] == 155000
    assert result[conn.getAccount(["Devisenhandel"])] == 0
    assert result[conn.getAccount(["Anfangsbestand"])] == 0
    assert result[conn.getAccount(["Erträge"])] == -160000
    assert result[conn.getAccount(["Fremdkapital"])] == -5000


def test_exchange():
    conn = GnuCashConnection(TEST_FILE, CLI_FILE_FORMAT)
    a_from = conn.getAccount(['Fremdkapital', 'Kreditkarte'])
    a_to = conn.getAccount(["Aktiva", "Bargeld"])
    assert conn.showBalance(a_from) == Decimal('-5000'), "Balance not ok before"
    assert conn.showBalance(a_to) == Decimal('0'), "Balance not ok before"
    conn.transactionExchange(a_from=a_from,
                             a_to=a_to,
                             amount=Decimal('100.00'),
                             tdate=datetime(int(2022), int(1), int(3), tzinfo=timezone.utc),
                             comment="Test Transaction test_exchange")
    assert conn.showBalance(a_from) == Decimal('-5100'), "Balance not ok after"
    assert conn.showBalance(a_to) == Decimal('100'), "Balance not ok after"


def test_extension():
    conn = GnuCashConnection(TEST_FILE, CLI_FILE_FORMAT)
    a_from = conn.getAccount(["Erträge", "Gehalt"])
    a_to = conn.getAccount(["Aktiva", "Bank"])
    assert conn.showBalance(a_from) == Decimal('-10000'), "Balance not ok before"
    assert conn.showBalance(a_to) == Decimal('10000'), "Balance not ok before"
    conn.transactionExtension(a_from=a_from,
                              a_to=a_to,
                              amount=Decimal('1500.00'),
                              tdate=datetime(int(2022), int(1), int(3), tzinfo=timezone.utc),
                              comment="Test Transaction test_extension")
    assert conn.showBalance(a_from) == Decimal('-8500'), "Balance not ok after"
    assert conn.showBalance(a_to) == Decimal('11500'), "Balance not ok after"
