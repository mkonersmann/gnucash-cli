# coding: utf-8
import os

from mko.gnucash_cli.GnuCashCLI import *
from click.testing import CliRunner

path = os.path.abspath(__file__)[0:-1 * len("test_GnuCashCLI.py")]
env = {'GNUCASH_CLI_FILE': path + 'gnucash_cli-test-file.gnucash'}


def test_show_1():
    runner = CliRunner()
    result = runner.invoke(show, ['Aktiva/Bank'], env=env)
    assert result.exit_code == 0
    assert result.output == '10000\n'


def test_show_2():
    runner = CliRunner()
    result = runner.invoke(show, ['Fremdkapital/Kreditkarte'], env=env)
    assert result.exit_code == 0
    assert result.output == '-5000\n'


def test_show_hierarchical():
    runner = CliRunner()
    result = runner.invoke(show, ['Fremdkapital'], env=env)
    assert result.exit_code == 0
    assert result.output == '-5000\n'


def test_show_all():
    runner = CliRunner()
    result = runner.invoke(showAll, env=env)
    assert result.exit_code == 0
    assert result.output == ' 160000.00 €\t\tErträge\n' \
           + '  10000.00 €\t\t\tGehalt\n' \
           + ' 150000.00 €\t\t\tGeschenke\n' \
           + ' 155000.00 €\t\tAufwendungen\n' \
           + '   5000.00 €\t\t\tGarten\n' \
           + ' 150000.00 €\t\t\tHaus\n' \
           + ' 150000.00 €\t\t\t\tBau\n' \
           + '      0.00 €\t\t\t\tWartung\n' \
           + '  10000.00 €\t\tAktiva\n' \
           + '  10000.00 €\t\t\tBank\n' \
           + '      0.00 €\t\t\tBargeld\n' \
           + '      0.00 €\t\t\tAktienkonto\n' \
           + '      0.00 €\t\t\tInvestmentfonds\n' \
           + '      0.00 €\t\t\tOffene Forderungen\n' \
           + '   5000.00 €\t\tFremdkapital\n' \
           + '      0.00 €\t\t\tOffene Verbindlichkeiten\n' \
           + '   5000.00 €\t\t\tKreditkarte\n' \
           + '      0.00 €\t\tAnfangsbestand\n' \
           + '      0.00 €\t\tDevisenhandel\n' \
           + '\n'


def test_exchange():
    runner = CliRunner()
    result = runner.invoke(exchange,
                           ["--dry-run", 'Aktiva/Bank', 'Aufwendungen/Garten', "1200", "Test Exchange", "2022-01-13"],
                           env=env)
    assert result.exit_code == 0
    assert len(result.output) == len(
        '49d1a3325694424680f3f0c70f90e65b\n')  # The guid is generated randomly. So we can only check the length.


def test_extension():
    runner = CliRunner()
    result = runner.invoke(extension,
                           ["--dry-run", "--", "Ertrag/Geschenke", 'Aktiva/Bargeld', "-200.50", "Test Extension", "2022-01-13"],
                           env=env)
    assert result.exit_code == 0
    assert len(result.output) == len(
        '49d1a3325694424680f3f0c70f90e65b\n')  # The guid is generated randomly. So we can only check the length.
